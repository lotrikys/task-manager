FROM python:3.11-alpine
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN apk add --no-cache build-base linux-headers && \
  pip install --no-cache-dir -r requirements.txt && \
  apk del build-base linux-headers
EXPOSE 8000
CMD ["uwsgi", "--ini", "uwsgi.ini"]
