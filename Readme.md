# Task Manager
Simple webservice for creating tasks. Based on [Django Soft Dashboard](https://github.com/app-generator/django-soft-ui-dashboard).

- [Task manager](https://task-manager.pp.ua/) - `Demo`

User - test/test12345

## Features:
- Login/Register user
- Crete/Update/Remove tasks
- Assign tasks to users
- Mark tasks complete/incomplete
- Set deadline for tasks

## Build and run
```bash
$ git clone git@gitlab.com:lotrikys/task-manager.git
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ python3 manage.py migrate
$ python3 manage.py createsuperuser
$ python3 manage.py runserver
```
For production use you need to define variables SECRET_KEY, DEBUG and ALLOWED_HOSTS by using environment
variables, .env or settings.ini files.

Environment variables example:
```bash
$ export SECRET_KEY=<your_secret_key>
$ export DEBUG=False
$ export ALLOWED_HOSTS='<your_domain>'
```

.env example:
```text
SECRET_KEY=<your_secret_key>
DEBUG=False
ALLOWED_HOSTS='<your_domain>'
```

settings.ini example:
```text
[settings]
SECRET_KEY=<your_secret_key>
DEBUG=False
ALLOWED_HOSTS='<your_domain>'
```

[Here](https://pypi.org/project/python-decouple/) you can read more about.

Open browser and go to http://127.0.0.1:8000/. Create new user via Sign Up button or login as superuser. Enjoy!