from django.urls import path

from dashboard.views import IndexView, ProfileView, WorkerCreateView, WorkerLoginView, ProfileEditView, TaskCreateView, \
    TaskView, TaskUpdateView, TaskDeleteView, task_mark_complete

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("profile/<int:pk>/", ProfileView.as_view(), name="profile"),
    path("profile/edit/<int:pk>/", ProfileEditView.as_view(), name="edit-profile"),
    path("register", WorkerCreateView.as_view(), name="register"),
    path("login", WorkerLoginView.as_view(), name="login"),
    path("task-create", TaskCreateView.as_view(), name="create-task"),
    path("task/<int:pk>/", TaskView.as_view(), name="task-view"),
    path("task/update/<int:pk>/", TaskUpdateView.as_view(), name="task-edit"),
    path("task/delete/<int:pk>/", TaskDeleteView.as_view(), name="task-delete"),
    path("task/mark-complete/<int:pk>/", task_mark_complete, name="task-complete"),
]


app_name = "dashboard"
