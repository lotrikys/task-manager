from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UsernameField

from dashboard.models import Worker, Task, Position


class WorkerRegistrationForm(UserCreationForm):
    password1 = forms.CharField(
        label="Password",
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}),
    )
    password2 = forms.CharField(
        label="Password Confirmation",
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password Confirmation'}),
    )
    first_name = forms.CharField(
        label="First name",
        widget=forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter your first name"})
    )
    last_name = forms.CharField(
        label="Last name",
        widget=forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter your last name"})
    )
    position = forms.ModelChoiceField(
        queryset=Position.objects.all(),
        label="Position",
        widget=forms.Select(attrs={"class": "form-select"})
    )

    class Meta:
        model = Worker
        fields = UserCreationForm.Meta.fields + ("position", "email", "first_name", "last_name")

        widgets = {
            'username': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Username'
            }),
            'email': forms.EmailInput(attrs={
                'class': 'form-control',
                'placeholder': 'Email',
                'required': 'true'
            }),
        }


class WorkerEditForm(forms.ModelForm):
    first_name = forms.CharField(
        label="First name",
        widget=forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter your first name"})
    )
    last_name = forms.CharField(
        label="Last name",
        widget=forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter your last name"})
    )

    class Meta:
        model = Worker
        fields = ("position", "email", "first_name", "last_name")

        widgets = {
            'email': forms.EmailInput(attrs={
                'class': 'form-control',
                'placeholder': 'Email',
                'required': 'true'
            }),
        }


class LoginForm(AuthenticationForm):
    username = UsernameField(widget=forms.TextInput(attrs={"class": "form-control", "placeholder": "Username"}))
    password = forms.CharField(
        label="Password",
        strip=False,
        widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": "Password"}),
    )


class TaskCreateForm(forms.ModelForm):
    name = forms.CharField(
        label="Task name",
        widget=forms.TextInput(attrs={"placeholder": "Enter task name"})
    )
    deadline = forms.DateField(
        label="Deadline",
        widget=forms.widgets.DateInput(
            format="%Y-%m-%d",
            attrs={"class": "form-control", "type": "date"}
        )
    )
    is_completed = forms.BooleanField(
        label="Completed?",
        required=False,
        widget=forms.CheckboxInput(attrs={"class": "border"})
    )
    assignees = forms.ModelMultipleChoiceField(
        queryset=Worker.objects.all(),
        label="Members",
        widget=forms.CheckboxSelectMultiple
    )

    class Meta:
        model = Task
        fields = "__all__"


class TaskEditForm(forms.ModelForm):
    deadline = forms.DateField(
        label="Deadline",
        widget=forms.widgets.DateInput(
            attrs={"class": "form-control", "type": "date"}
        )
    )
    is_completed = forms.BooleanField(
        label="Completed?",
        required=False,
        widget=forms.CheckboxInput(attrs={"class": "border"})
    )
    assignees = forms.ModelMultipleChoiceField(
        queryset=Worker.objects.all(),
        label="Members",
        widget=forms.CheckboxSelectMultiple
    )

    class Meta:
        model = Task
        fields = ("description", "deadline", "is_completed", "priority", "task_type", "assignees")


class TaskSearchForm(forms.Form):
    name = forms.CharField(
        max_length=255,
        required=False,
        label="",
        widget=forms.TextInput(attrs={
            "placeholder": "Search by task name...",
            "class": "form-control"
        })
    )
