from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin

from dashboard.forms import WorkerRegistrationForm, LoginForm, WorkerEditForm, TaskCreateForm, TaskEditForm, \
    TaskSearchForm
from dashboard.models import Task, Worker


class IndexView(LoginRequiredMixin, generic.ListView):
    model = Task
    template_name = "pages/index.html"

    def get_queryset(self):
        form = TaskSearchForm(self.request.GET)

        if form.is_valid() and form.cleaned_data["name"]:
            return Task.objects.filter(
                assignees__id=self.request.user.id,
                name__icontains=form.cleaned_data["name"]
            )

        return Task.objects.filter(assignees__id=self.request.user.id)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        name = self.request.GET.get("name", "")

        context["search_form"] = TaskSearchForm(initial={"name": name})

        context["segment"] = "dashboard"

        return context


class ProfileView(LoginRequiredMixin, generic.DetailView):
    model = Worker
    template_name = "pages/profile.html"
    context_object_name = "worker"

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)

        context["segment"] = "profile"
        context["tasks"] = Task.objects.filter(assignees__id=self.kwargs["pk"])

        return context

    def get(self, request, *args, **kwargs):
        if request.user.id == self.kwargs["pk"] or request.user.is_superuser:
            self.object = self.get_object()
            context = self.get_context_data(object=self.object)
            return self.render_to_response(context)
        else:
            return HttpResponseRedirect(reverse_lazy("dashboard:index"))


class ProfileEditView(LoginRequiredMixin, generic.UpdateView):
    model = Worker
    form_class = WorkerEditForm
    template_name = "pages/profile.html"
    context_object_name = "worker"

    def get_context_data(self, **kwargs):
        context = super(ProfileEditView, self).get_context_data(**kwargs)

        context["segment"] = "profile"
        context["mode"] = "edit"
        context["tasks"] = Task.objects.filter(assignees__id=self.kwargs["pk"])

        return context

    def get(self, request, *args, **kwargs):
        if request.user.id == self.kwargs["pk"] or request.user.is_superuser:
            self.object = self.get_object()
            context = self.get_context_data(object=self.object)
            return self.render_to_response(context)
        else:
            return HttpResponseRedirect(reverse_lazy("dashboard:index"))

    def get_success_url(self):
        return reverse_lazy("dashboard:profile", kwargs={"pk": self.kwargs["pk"]})


class WorkerCreateView(generic.CreateView):
    model = Worker
    form_class = WorkerRegistrationForm
    template_name = "registration/register.html"
    success_url = reverse_lazy("dashboard:index")

    def form_valid(self, form):
        to_return = super().form_valid(form)
        user = authenticate(
            username=form.cleaned_data["username"],
            password=form.cleaned_data["password1"],
        )
        login(self.request, user)
        return to_return


class WorkerLoginView(LoginView):
    template_name = "registration/login.html"
    success_url = reverse_lazy("dashboard:index")
    form_class = LoginForm


class TaskCreateView(LoginRequiredMixin, generic.CreateView):
    model = Task
    form_class = TaskCreateForm
    template_name = "pages/task_create.html"
    success_url = reverse_lazy("dashboard:index")

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(TaskCreateView, self).get_context_data(**kwargs)

        context["segment"] = "create-task"

        return context


class TaskView(LoginRequiredMixin, generic.DetailView):
    model = Task
    template_name = "pages/task.html"
    context_object_name = "task"

    def get_context_data(self, **kwargs):
        context = super(TaskView, self).get_context_data(**kwargs)

        context["segment"] = "task-view"

        return context


@login_required
def task_mark_complete(request, pk):
    Task.objects.filter(id=pk).update(is_completed=True)
    return HttpResponseRedirect(reverse_lazy("dashboard:task-view", kwargs={"pk": pk}))


class TaskUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = Task
    form_class = TaskEditForm
    template_name = "pages/task.html"
    context_object_name = "task"

    def get_context_data(self, **kwargs):
        context = super(TaskUpdateView, self).get_context_data(**kwargs)

        context["segment"] = "task-edit"
        context["mode"] = "edit"

        return context

    def get_success_url(self):
        return reverse_lazy("dashboard:task-view", kwargs={"pk": self.kwargs["pk"]})


class TaskDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Task
    context_object_name = "task"
    template_name = "pages/task_confirm_delete.html"
    success_url = reverse_lazy("dashboard:index")

    def get_context_data(self, **kwargs):
        context = super(TaskDeleteView, self).get_context_data(**kwargs)

        context["segment"] = "task"

        return context
